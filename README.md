# Coding style for Javascript/Typescript/React <!-- omit in toc -->

As a goal to standardize and to make our code readable and maintainable, here's a small style guide to use.

>---
>
> **There are no "you must" rules.**
>
> Nothing is set in stone. These are style preferences, not religious dogmas.
> 
> Please let me know if you disagree with some of these rules.
>
> You are welcome to do an MR if you see any mistakes and/or disagree with some
>
>---

<br />

- See [Coding style](coding.md)