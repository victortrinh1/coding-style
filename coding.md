# Coding style for Javascript/Typescript/React <!-- omit in toc -->

As a goal to standardize and to make our code readable and maintainable, here's a small style guide to use.

>---
>
> **There are no "you must" rules.**
>
> Nothing is set in stone. These are style preferences, not religious dogmas.
> 
> Please let me know if you disagree with some of these rules.
>
> You are welcome to do an MR if you see any mistakes and/or disagree with some styles.
>
>---

<br />

---

## Table of Contents <!-- omit in toc -->
 
- [1. Naming](#1-naming)
  - [1.1. Functions and variables](#11-functions-and-variables)
  - [1.2. Components and their associated filenames](#12-components-and-their-associated-filenames)
  - [1.3. Interface and Type](#13-interface-and-type)
  - [1.4. Namespace](#14-namespace)
  - [1.5. Enum](#15-enum)
- [2. Curly braces](#2-curly-braces)
- [3. Early exit](#3-early-exit)
- [4. References](#4-references)
- [5. Spacing](#5-spacing)
- [6. Props](#6-props)
- [7. Tags](#7-tags)
- [8. Null vs. Undefined](#8-null-vs-undefined)
- [9. Type vs. Interface](#9-type-vs-interface)
- [10. Destructuring](#10-destructuring)
- [11. Ordering](#11-ordering)
  - [11.1. FunctionComponent](#111-functioncomponent)
  - [11.2. Component and PureComponent](#112-component-and-purecomponent)
- [12. Avoid magic strings](#12-avoid-magic-strings)
- [13. Avoid polluting connections](#13-avoid-polluting-connections)
- [14. Avoid casting the TypeScript types](#14-avoid-casting-the-typescript-types)
- [15. Avoid Lodash's get function](#15-avoid-lodashs-get-function)
- [16. Avoid the TypeScript's non-null assertion operator (someVar!)](#16-avoid-the-typescripts-non-null-assertion-operator-somevar)
- [17. Write meaningful error messages](#17-write-meaningful-error-messages)
- [18. Isolate specific computations instead of a complete resolver](#18-isolate-specific-computations-instead-of-a-complete-resolver)

---

## 1. Naming

### 1.1. Functions and variables

- Use `camelCase`. eslint: [camelcase](https://eslint.org/docs/rules/camelcase)


❌ **Bad**
```typescript
const FooVar;
function BarFunc() {}
```
✔ **Good**
```typescript
const fooVar;
function barFunc() {}
```

---

### 1.2. Components and their associated filenames

- Use `PascalCase`. eslint: [react/jsx-pascal-case](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-pascal-case.md)

**Filename**

❌ **Bad**
```typescript
catTile.tsx;
```

✔ **Good**
```typescript
CatTile.tsx;
```

---

**Reference naming** 
- Use `PascalCase`. eslint: [react/jsx-pascal-case](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-pascal-case.md)
- Use camelCase for their instances. See [Functions and variables](#11-functions-and-variables)

❌ **Bad**
```typescript
import catTile from "./CatTile";
const CatTile = <CatTile />;
```

✔ **Good**
```typescript
import CatTile from "./CatTile";
const catTile = <CatTile />;
```

---

### 1.3. Interface and Type

- Use `PascalCase` for name
- Use `camelCase` for members
- **Don't** prefix with `I`

> Reason: lib.d.ts defines important interfaces without an `I` (e.g. Window, Document, etc)

❌ **Bad**
```typescript
interface ICat {}
type cat = {};
```

✔ **Good**
```typescript
interface Cat {}
type Cat = {};
```

---

### 1.4. Namespace

- Use `PascalCase`

❌ **Bad**
```typescript
namespace cat {}
```

✔ **Good**
```typescript
namespace Cat {}
```

---

### 1.5. Enum

- Use `PascalCase` for name

❌ **Bad**
```typescript
enum cat {}
```

✔ **Good**
```typescript
enum Cat {}
```

- Use `PascalCase` for enum member

❌ **Bad**
```typescript
enum Cat {
  siamese,
}
```

✔ **Good**
```typescript
enum Cat {
  Siamese,
}
```

[↥ back to top](#)

---

## 2. Curly braces

- Use "Egyptian" style with the opening brace on the same line as the corresponding keyword. There should also be a space before the opening bracket. eslint: [curly](https://eslint.org/docs/rules/curly)

Example:

```typescript
if (condition) {
  // do this
  // ...and that
}
```

A single-line construct, such as `if (condition) doSomething()`, is an important edge case. For simplicity, readability and maintainability, a code block is usually better.

❌ **Bad**
```typescript
if (condition) doSomething();

while (condition) doSomething();

if (condition) {
  doSomething();
} else doSomethingElse();
```

✔ **Good**
```typescript
if (condition) {
  doSomething();
}

while (condition) {
  doSomething();
}

if (condition) {
  doSomething();
} else {
  doSomethingElse();
}
```

[↥ back to top](#)

---

## 3. Early exit

- Favor early exit to avoid nesting code.

> Loops: Use `continue` directive.

❌ **Bad**
```typescript
foreach (const cat in cats) {
  if (cont) {
     ... // One more nesting level
  }
}
```

✔ **Good**
```typescript
foreach (const cat in cats) {
  if (!cont) {
     continue;
  }

  // No extra nesting level
}
```

> if/else: Use `return` directive.

❌ **Bad**
```typescript
function cat(sound: string) {
  if (sound !== "meow") {
    alert("Not a cat!");
  } else {
    doLotsOfCalculations();

    return "It's a cat";
  }
}
```

✔ **Good**
```typescript
function cat(sound: string) {
  if (sound !== "meow") {
    alert("Not a cat!");
    return;
  }

  doLotsOfCalculations();

  return "It's a cat";
}
```

The special cases are handled early on. Once the check is done, we can move to the "main" code flow without the need for additional nesting.

[↥ back to top](#)

---

## 4. References

- Use `const` for all of your references; avoid using var. eslint: [prefer-const](https://eslint.org/docs/rules/prefer-const.html), [no-const-assign](https://eslint.org/docs/rules/no-const-assign.html)

❌ **Bad**
```typescript
var a = 1;
var b = 2;
```

✔ **Good**
```typescript
const a = 1;
const b = 2;
```

- If you must reassign references, use `let` instead of `var`. eslint: [no-var](https://eslint.org/docs/rules/no-var.html)

❌ **Bad**
```typescript
var count = 1;

if (true) {
  count += 1;
}
```

✔ **Good**
```typescript
let count = 1;

if (true) {
  count += 1;
}
```

The main reason is that `let` and `const` are block-scoped which can prevent bugs.

[↥ back to top](#)

---

## 5. Spacing

- Include a single space in your self-closing tag. eslint: [no-multi-spaces](https://eslint.org/docs/rules/no-multi-spaces), [react/jsx-tag-spacing](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-tag-spacing.md)

❌ **Bad**
```typescript
<Cat/>
<Cat
 />

// Very bad
<Cat      />
```

✔ **Good**
```typescript
<Cat />
```

- Do not pad JSX curly braces with spaces. eslint: [react/jsx-curly-spacing](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-curly-spacing.md)

❌ **Bad**
```typescript
<Cat sound={ meow } />;
```

✔ **Good**
```typescript
<Cat sound={meow} />;
```

[↥ back to top](#)

---

## 6. Props

- Use camelCase for prop names

❌ **Bad**
```typescript
<Cat daily_action={eat} Sound="meow" />;
```

✔ **Good**
```typescript
<Cat dailyAction={eat} sound="meow" />;
```

- Alphabetize the props for readability. eslint: [react/jsx-sort-props](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-sort-props.md)

❌ **Bad**
```typescript
<Cat lastName="Smith" firstName="John" />;
```

✔ **Good**
```typescript
<Cat firstName="John" lastName="Smith" />;
```

- Omit the value of prop when it is explicitly `true`. eslint: [react/jsx-boolean-value](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-boolean-value.md)

❌ **Bad**
```typescript
<Cat cute={true} />;
```

✔ **Good**
```typescript
<Cat cute />;
```

[↥ back to top](#)

---

## 7. Tags

- Self-close tags that have no children. eslint: [react/self-closing-comp](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/self-closing-comp.md)

❌ **Bad**
```typescript
<Cat sound="meow"></Cat>;
```

✔ **Good**
```typescript
<Cat sound="meow" />;
```

- If the component has multi-line properties, close its tag on a new line. eslint: [react/jsx-closing-bracket-location](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-closing-bracket-location.md)

❌ **Bad**
```typescript
<Cat 
  sound="meow" 
  dailyAction={eat} />;
```

✔ **Good**
```typescript
<Cat 
  sound="meow" 
  dailyAction={eat} 
/>;
```

[↥ back to top](#)

---

## 8. Null vs. Undefined

- Use _truthy_ check for objects being `null` or `undefined`

❌ **Bad**
```typescript
if (error === null)
```

✔ **Good**
```typescript
if (error)
```

- Use `== null` / `!= null` not (`===` / `!==`) to check for `null` / `undefined` on primitives as it works for both `null` / `undefined` but not other falsy values (like `''`, `0`, `false`)

❌ **Bad**
```typescript
if (error !== null) // does not rule out undefined
```

✔ **Good**
```typescript
if (error != null) // rules out both null and undefined
```

[↥ back to top](#)

---

## 9. Type vs. Interface

- Use `type` when you _might_ need a union or intersection

```typescript
type Cat = string | { sound: string };
```

- Use `interface` when you want `extends` or `implements`

```typescript
interface Cat {
  sound: string;
}

interface Siamese extends Cat {
  stripes: string;
}

class Harry implements Siamese {
  sound: string;
  stripes: string;
}
```

- Otherwise, use whatever makes you happy.

[↥ back to top](#)

---

## 10. Destructuring

- Use object destructuring when accessing and using multiple properties of an object. eslint: [prefer-destructuring](https://eslint.org/docs/rules/prefer-destructuring)

❌ **Bad**
```typescript
function getFullName(user) {
  const firstName = user.firstName;
  const lastName = user.lastName;

  return `${firstName} ${lastName}`;
}
```

✔ **Good**
```typescript
function getFullName(user) {
  const { firstName, lastName } = user;

  return `${firstName} ${lastName}`;
}
```

✔ **Best**
```typescript
function getFullName({ firstName, lastName }) {
  return `${firstName} ${lastName}`;
}
```

- Use array destructuring. eslint: [prefer-destructuring](https://eslint.org/docs/rules/prefer-destructuring)

```typescript
const array = [1, 2, 3, 4];

// Bad
const first = array[0];
const second = array[1];

// Good
const [first, second] = array;
```

[↥ back to top](#)

---

## 11. Ordering

- Do prioritize function components to be able to use hooks.
- Only one component should be exported per file usually.

### 11.1. FunctionComponent

1. Imports
2. Any Constants if needed to avoid magic strings/numbers in code
3. Typed props
4. The function component
   1. Destructed used props in parameters
   2. Any hooks in the order that makes sense (useState, useEffect)
   3. clickHandlers or eventHandlers
   4. getter methods for render
   5. optional render methods
   6. Return
5. Export component

```typescript
import { useState } from "react";

const CAT_SOUND = "meow";

type CatProps = {
  sound: string;
};

const Cat = ({ sound }: CatProps) => {
  const [clicked, setClicked] = useState<number>(0);

  const onClick = () => {
    setClicked(clicked++);
  };

  if (sound !== CAT_SOUND) {
    return null;
  }

  return (
    <div onClick={onClick}>{`${sound}: This was clicked ${clicked} times`}</div>
  );
};

export default Cat;
```

---

### 11.2. Component and PureComponent

Some of these might be _obsolete_

1. Imports
2. Any constants to avoid magic strings/numbers
3. Typed props
4. Component
   1. optional static methods
   2. constructor
   3. getChildContext
   4. componentWillMount
   5. componentDidMount
   6. componentWillReceiveProps
   7. shouldComponentUpdate
   8. componentWillUpdate
   9. componentDidUpdate
   10. componentWillUnmount
   11. clickHandlers or eventHandlers
   12. getter methods for render
   13. optional render methods
   14. render
   15. destructured used props and state

```typescript
import React from "react";

const CAT_SOUND = "meow";

type CatProps = {
  sound: string;
};

type CatState = {
  clicked: number;
};

export default class Cat extends React.Component<CatState, CatProps> {
  static methodsAreOk() {
    return true;
  }

  constructor(props) {
    super(props);
    this.state = {
      clicked: 0
    };
  }

  onClick = () => {
    this.setState((prevstate) => ({ clicked: prevState.clicked++ }));
  };

  render() {
    const { sound } = this.props;
    const { clicked } = this.state;

    if (sound !== CAT_SOUND) {
      return null;
    }

    return (
      <div onClick={this.onClick}>
        {`${sound}: This was clicked ${clicked} times`}
      </div>
    );
  }
}
```

[↥ back to top](#)

---

## 12. Avoid magic strings

- Use an enum instead of magic strings, so it can be typechecked!

❌ **Bad** - Avoid string literals
```typescript
// types.ts
type Avatar = {
  type: 'Profile' | 'Space';
  //...
};

// in some resolver:
if (avatar.type !== 'Space') //...
```

✔ **Good**
```typescript
// types.ts
export enum AvatarType {
  Profile = 'Profile';
  Space = 'Space';
}

type Avatar = {
  type: AvatarType;
  //...
};

// in some resolver, type checked value!
if (avatar.type === AvatarType.Space) //...
```

[↥ back to top](#)

---


## 13. Avoid polluting connections

- Use custom types instead of polluting connections

**Example type**
```typescript
type GamesConnection {
  // Only exists to satisfy the type system, isn't needed and shouldn't be queried.
  spaceIds: [String!]!

  // Uses `spaceIds`, so the type system complains is not defined in the connection type.
  nodes: [Game!]!
}
```

❌ **Bad** 
```typescript
export const resolvers: {
  GamesConnection: GamesConnectionResolvers<
    UpnContextType, // our custom context type.
    // An incomplete custom parent type
    Pick<GamesConnection, "userId" | "spaceIds">
  >;
} = {
  // resolvers here...
};
```

✔ **Good**
```typescript
export const resolvers: {
  GamesConnection: GamesConnectionResolvers<
    UpnContextType, // our custom context type.
    // Our extended custom parent type
    Pick<GamesConnection, "userId"> & {
      spaceIds: string[];
    }
  >;
} = {
  // resolvers here...
};
```

[↥ back to top](#)

---

## 14. Avoid casting the TypeScript types

> Reason: Casting a value to the correct type completly ignores type checks and TypeScript puts its complete trust on us, the developers, not to fail, which more often than not, when casting, means that we already failed.

❌ **Bad** 
```typescript
// completely ignores the type problem and might return a value
// that is not within the `EnumType` possible values.
return myString as EnumType;
```

✔ **Good**
```typescript
// First validate the data.
if (!Object.values<string>(EnumType).includes(myString)) {
  // Early failure if the data is really not within the type we were expecting.
  throw new ApolloError(`Unsupported EnumType value: '${myString}'.`);
}

// Now we're sure of the type!
return myString;
```

[↥ back to top](#)

---

## 15. Avoid Lodash's get function

> Reason: Using Lodash's get function returns "any" type, which bypasses further type validations.

❌ **Bad** 
```typescript
const spaceId = _.get(someData, "spaceId", null);
// spaceId: any;
```

✔ **Good**
```typescript
const spaceId = "spaceId" in someData ? someData.spaceId : null;
// spaceId: string | null;
```

[↥ back to top](#)

---

## 16. Avoid the TypeScript's non-null assertion operator (someVar!)

> Reason: If TypeScript complains that a type could be null, but you know that it can't, don't use the ! operator to silence the error and instead fix the underlying types.

❌ **Bad** 
```typescript
type Headers = {
  appId?: string; // or string | undefined
};

// Using the non-null assertion.
const applicationId = headers.appId!;
// applicationId: string; but still could be null at runtime.
```

✔ **Good**
```typescript
type Headers = {
  appId: string; // explicitely a string.
};

// E.g. validate headers at init time.
if (!headers.appId) {
  throw new ApolloError("The Ubi-AppId header is required.");
}

const applicationId = headers.appId;
// applicationId: string;
// We're now sure there's an appId in the headers.
```

[↥ back to top](#)

---

## 17. Write meaningful error messages

> Keep in mind that error messages are meant for the client apps developers. It should help them understand where the error comes from and if there's something wrong on their side or if it's a bug on the server or the services.

❌ **Bad** 
```typescript
// These are unhelpful at best and frustrating at worst.
throw new ApolloError(`Unkown type: ${id}`);
```

✔ **Good**
```typescript
// Write a complete sentence, with a clear format, quoting any dynamic data so it's clear if we ever received an empty string.
throw new ApolloError(
  `Unsupported periodic challenge type: '${challenge.type}'.`
);
```

[↥ back to top](#)

---

## 18. Isolate specific computations instead of a complete resolver

> Sometimes, we end up repeating the same logic in a couple resolvers. It's tempting to just put the resolver function into some helper file and be done with it.

❌ **Bad** 
```typescript
// While isolating a resolver sounds like a good idea at first, it limits reusability later on when a new use-case arises where the input of the resolver's computation must change.

// If I already got the userReward object, this resolver becomes way less useful.
export const isRedeemed = async (
  { spaceId, userId, rewardId },
  arg,
  { dataSources }
) => {
  const { status } = await dataSources.rewardAPI.userReward.load({
    spaceId,
    rewardId,
    userId,
  });
  return status === UserRewardStatus.Purchased;
};

```

✔ **Good**
```typescript
// It reduces noise in the resolvers files, makes it easy to reuse and improves the maintainability by having only one place to manage the logic.

// Reusable computation.
export const isRewardRedeemed = ({ status }: UserRewardResponse) =>
  status === UserRewardStatus.Purchased;

// The resolver becomes super simple.
const resolvers = {
  isRedeemed: ({ spaceId, userId, rewardId }, arg, { dataSources }) =>
    dataSources.rewardAPI.userReward
      .load({
        spaceId,
        rewardId,
        userId,
      })
      .then(isRewardRedeemed),
};

```

[↥ back to top](#)

---

## Built with the help of other style guides <!-- omit in toc -->

- [Airbnb Javascript Style Guide](https://github.com/airbnb/javascript)
- [Airbnb React/JSX Style Guide](https://airbnb.io/javascript/react)
- [Typescript Deep Dive Style Guide](https://basarat.gitbook.io/typescript/styleguide)
- [Google Javascript Style Guide](https://google.github.io/styleguide/jsguide.html)
- [JavaScript Standard Style](https://standardjs.com)
- [Javascript Coding Style](https://javascript.info/coding-style)
